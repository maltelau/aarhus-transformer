"""
Downloading and preparing test sets
Originally danish ones via aleksandra instittuttet.


Each function returns a tuple of (train,dev,test) Datasets
"""

import time
from danlp.datasets import DDT
# from googletrans import Translator

from operator import add
from functools import reduce
from collections import namedtuple

import pandas as pd



Dataset = namedtuple("Dataset", ["x", "y"])
    
# ###################### NER #################################
# https://github.com/alexandrainst/danlp/blob/master/examples/benchmarks/ner_benchmarks.py


# NER tagging to four different categories: LOC, ORG, PER, MISC. We
# filter out MISC because it's "of limited use" according to the
# aleksandra institute benchmark
def _is_misc(ent: str):
    if len(ent) < 4:
        return False
    return ent[-4:] == 'MISC'


def _simplener_to_pandas(ind):
    step1 = ([(i,word,ner) for  word, ner in zip(t_sent, t_ner)] for i, (t_sent, t_ner) in enumerate(zip(*ind)))
    step2 = reduce(add, step1)
    step25 = filter(lambda row: not _is_misc(row[2]), step2)
    step3 = pd.DataFrame(step25, columns = ["sentenceid", "text", "ner"])
    return step3


def data_ner():
    ddt = DDT()
    train, dev, test = ddt.load_as_simple_ner(predefined_splits=True)

    train = Dataset.from_pandas(_simplener_to_pandas(train))
    test = Dataset.from_pandas(pd.concat([_simplener_to_pandas(dev),
                                          _simplener_to_pandas(test)]))

    return train, test


# ###################### POS #################################
# https://github.com/alexandrainst/danlp/blob/master/examples/benchmarks/pos_benchmarks.py


def _conllu_as_pandas(ind):
    step1 = ([(tok.form, tok.upos) for tok in sent] for sent  in ind)
    step2 = reduce(add, step1)
    step3 = pd.DataFrame(step2, columns = ["text", "pos"])
    # step4 = Dataset.from_pandas(step3)
    return step3


def data_pos():
    ddt = DDT()
    train, dev, test = ddt.load_as_conllu(predefined_splits=True)
    train = Dataset.from_pandas(_conllu_as_pandas(train))
    test = Dataset.from_pandas(pd.concat([_conllu_as_pandas(dev),
                                          _conllu_as_pandas(test)]))

    return train, test


# ###################### Sentiment #################################
# https://github.com/alexandrainst/danlp/blob/master/examples/benchmarks/sentiment_benchmark.py


from danlp.datasets import EuroparlSentiment1, EuroparlSentiment2, LccSentiment   ## noqa


def sentiment_score_to_label(score):
    if score == 0:
        return 'neutral'
    if score < 0:
        return 'negativ'
    else:
        return 'positiv'
    

def data_sentiment():
    # ## dev
    eu = EuroparlSentiment2().load_with_pandas()
    # #### plus annotated tweets
    # https://github.com/alexandrainst/danlp/blob/master/examples/benchmarks/sentiment_benchmark_twitter.py
    
    # dev = Dataset.from_pandas(eu)
    dev = Dataset(list(eu.text.values), list(eu.polarity.values))
    
    # ## test
    eu = EuroparlSentiment1().load_with_pandas()
    lc = LccSentiment().load_with_pandas()
    
    test = pd.concat([eu,lc])
    test = Dataset(list(test.text.values),
                   [sentiment_score_to_label(s) for s in test.valence.values])

    return dev, test

# ###################### Word embedding similarity ############################
# https://github.com/alexandrainst/danlp/blob/master/examples/benchmarks/wordembeddings_benchmarks.py
from danlp.datasets import WordSim353Da, DSD   ## noqa

def data_embedding():
    ws353 = WordSim353Da().load_with_pandas()
    dsd = DSD().load_with_pandas()
    dsd.rename({"da1": "word1", "da2": "word2", "Human (mean)": "similarity"}, inplace = True)
    
    test = Dataset.from_pandas(pd.concat([ws353, dsd]))

    return None, test

# #### producing BERT word embeddings:
# https://medium.com/analytics-vidhya/bert-word-embeddings-deep-dive-32f6214f02bf


# ###################### multiNLI (machine-translated) ########################
# https://cims.nyu.edu/~sbowman/multinli/

def _trans(text):
    # try:
    from googletrans import Translator
    translator = Translator()
    result = translator.translate(text, src="en", dest="da")
    del translator
    return result


def _translate_xnli(obs):
    if len(obs['hypothesis']) == 2:
        # TODO: WHY??!? is it being called an extra time at the start?
        return obs

    res = None
    while not res:
        try:
            res = _trans([obs['hypothesis'], obs['premise']])
        except AttributeError:
            print("Error, trying again in 5 seconds.")
            time.sleep(5)
    hypo, premise = res
    result = {}
    result['hypothesis'] = [h.text for h in hypo]
    result['premise'] = [p.text for p in premise]
    return result

def data_xnli():

    xnli = load_dataset("xnli", "en", split=["train", "validation+test"])
    # test with only 3 rows for now
    # x_translated = xnli['test'].select([0, 1, 2]).map(_translate_xnli,
    #                                                   batched=True, batch_size=10)

    return xnli[0], xnli[1]


# ###################### SQUAD #################################
# https://rajpurkar.github.io/SQuAD-explorer/


def _translate_squad(obs):
    if len(obs['question']) == 2:
        return obs
    res = None
    while not res:
        try:
            res = _trans([obs['context'], obs['question'],
                         [o['text'] for o in obs['answers']]])
        except AttributeError:
            print("Error, trying again in 5 seconds.")
            time.sleep(5)

    result = {}
    result['context'] = [r.text for r in res[0]]
    result['question'] = [r.text for r in res[1]]
    result['answers'] = [{'text': [a.text for a in r]} for r in res[2]]
    return result


def data_squad():
    squad = load_dataset("squad")
    # test with only 3 rows for now
    # s_translated = squad['train'].select([0,1,2]).map(_translate_squad,
    #                                                   batched=True, batch_size=10)

    return squad['train'], squad['validation']




#################### dependency parsing? #############################
# http://docs.deeppavlov.ai/en/master/features/models/syntaxparser.html
