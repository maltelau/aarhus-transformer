from build_test_data import data_sentiment
import tensorflow as tf
from transformers import BertTokenizerFast, TFBertForSequenceClassification, TFTrainer, TFTrainingArguments

train, test = data_sentiment()

tokenizer = BertTokenizerFast.from_pretrained('bert-base-multilingual-cased')
train_encodings = tokenizer(train.x, truncation=True, padding=True)
test_encodings = tokenizer(test.x, truncation=True, padding=True)

def to_binary(y):
    return [0 if case == "neutral" else 1 for case in y]


train_dataset = tf.data.Dataset.from_tensor_slices((
    dict(train_encodings),
    to_binary(train.y)
))
test_dataset = tf.data.Dataset.from_tensor_slices((
    dict(test_encodings),
    to_binary(test.y)
))


training_args = TFTrainingArguments(
    output_dir='./results',          # output directory
    num_train_epochs=3,              # total number of training epochs
    per_device_train_batch_size=16,  # batch size per device during training
    per_device_eval_batch_size=64,   # batch size for evaluation
    warmup_steps=500,                # number of warmup steps for learning rate scheduler
    weight_decay=0.01,               # strength of weight decay
    logging_dir='./logs',            # directory for storing logs
    logging_steps=10,
)

with training_args.strategy.scope():
    model = TFBertForSequenceClassification.from_pretrained("bert-base-multilingual-cased")

trainer = TFTrainer(
    model=model,                         # the instantiated 🤗 Transformers model to be trained
    args=training_args,                  # training arguments, defined above
    train_dataset=train_dataset,         # training dataset
    eval_dataset=test_dataset             # evaluation dataset
)

trainer.train()
print(trainer)
print(model)
